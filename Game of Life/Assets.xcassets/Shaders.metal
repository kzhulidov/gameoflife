
#include <metal_stdlib>
using namespace metal;

int countNeighbors(device const bool* world,
                   uint2 id,
                   uint xMax) {
    const int num_cells = 8;
    const int x_coord[] = {-1, 0, 1, 1, 1, 0, -1, -1};
    const int y_coord[] = {-1, -1, -1, 0, 1, 1, 1, 0};
    
    int neighbors = 0;
    for (int i = 0; i < num_cells; i++) {
      neighbors += world[id.x + x_coord[i] + (id.y + y_coord[i]) * xMax];
    }

    return neighbors;
}

kernel void evolve(device const bool* oldWorld [[buffer(0)]],
                   device bool* newWorld [[buffer(1)]],
                   uint2 id0 [[thread_position_in_grid]],
                   uint2 size [[threads_per_grid]]) {
    const uint2 id1 = id0 + uint2(1, 1);
    const uint xMax = size.x + 2;
    const uint index = id1.x + id1.y * xMax;
    const int neighbors = countNeighbors(oldWorld, id1, xMax);
    
    bool cell = oldWorld[index];
    if ((neighbors < 2) || (neighbors > 3)) {
        cell = 0;
    }
    else if (neighbors == 3) {
        cell = 1;
    }
    
    newWorld[index] = cell;
}

struct DrawParams {
    uint cellsPerRow;
    uint scale;
};

kernel void draw(constant DrawParams* params [[buffer(0)]],
                 device const bool* world [[buffer(1)]],
                 texture2d<float, access::write> output [[texture(2)]],
                 uint2 id [[thread_position_in_grid]]) {
    const uint index = id.x / params->scale + (id.y / params->scale) * params->cellsPerRow;
    
    output.write(world[index] ? 1000 : 0, id);
}
