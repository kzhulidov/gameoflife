
import MetalKit

public class Renderer: NSObject, MTKViewDelegate {
    
    public var device: MTLDevice!
    var queue: MTLCommandQueue!
    var pipelineStateEvolve: MTLComputePipelineState!
    var pipelineStateDraw: MTLComputePipelineState!
    var world1: MTLBuffer!
    var world2: MTLBuffer!
    let sizeX = 1000
    let sizeY = 1000
    var generation = 0

    struct DrawParams {
        var cellsPerRow: UInt32
        var scale: UInt32
    }
    
    override public init() {
        super.init()
        initializeMetal()
    }

    func initializeMetal() {
        device = MTLCreateSystemDefaultDevice()
        queue = device.makeCommandQueue()
        do {
            let library = device.makeDefaultLibrary()!
            let functionEvolve = library.makeFunction(name: "evolve")!
            pipelineStateEvolve = try device.makeComputePipelineState(function: functionEvolve)
            let functionDraw = library.makeFunction(name: "draw")!
            pipelineStateDraw = try device.makeComputePipelineState(function: functionDraw)
        } catch let error {
            print(error.localizedDescription)
        }
        allocateWorlds(nrInitialCells: 200000)
    }

    func allocateWorlds(nrInitialCells: Int) {
        let count = sizeX * sizeY
        var vector = [Bool](repeating: false, count: count)
        let length = count * MemoryLayout<Bool>.stride
        
        // Put some cells in
        for _ in 0..<nrInitialCells {
            vector[Int.random(in: 0..<count)] = true;
        }
        
        world1 = device.makeBuffer(bytes: vector, length: length, options: [])
        world2 = device.makeBuffer(bytes: vector, length: length, options: [])
    }
    
    func encodeEvolve(with commandEncoder: MTLComputeCommandEncoder) {
        commandEncoder.setComputePipelineState(pipelineStateEvolve)
        commandEncoder.setBuffer(generation % 2 == 1 ? world2 : world1, offset: 0, index: 0)
        commandEncoder.setBuffer(generation % 2 == 1 ? world1 : world2, offset: 0, index: 1)
        
        let width = pipelineStateEvolve.threadExecutionWidth
        let height = pipelineStateEvolve.maxTotalThreadsPerThreadgroup / width
        let threadsPerGroup = MTLSizeMake(width, height, 1)
        let threadsPerGrid = MTLSizeMake(sizeX - 2, sizeY - 2, 1)
        commandEncoder.dispatchThreads(threadsPerGrid, threadsPerThreadgroup: threadsPerGroup)
    }
    
    func encodeDraw(to texture: MTLTexture, with commandEncoder: MTLComputeCommandEncoder) {
        commandEncoder.setComputePipelineState(pipelineStateDraw)
        var params = DrawParams(cellsPerRow: UInt32(sizeX), scale: 10)
        commandEncoder.setBytes(&params, length: MemoryLayout<DrawParams>.size, index: 0)
        commandEncoder.setBuffer(generation % 2 == 1 ? world1 : world2, offset: 0, index: 1)
        commandEncoder.setTexture(texture, index: 2)
        
        let width = pipelineStateDraw.threadExecutionWidth
        let height = pipelineStateDraw.maxTotalThreadsPerThreadgroup / width
        let threadsPerGroup = MTLSizeMake(width, height, 1)
        let threadsPerGrid = MTLSizeMake(texture.width, texture.height, 1)
        commandEncoder.dispatchThreads(threadsPerGrid, threadsPerThreadgroup: threadsPerGroup)
    }
    
    public func draw(in view: MTKView) {
        guard let commandBuffer = queue.makeCommandBuffer(),
              let commandEncoder = commandBuffer.makeComputeCommandEncoder(),
              let drawable = view.currentDrawable else {
            return
        }
        
        generation += 1
        
        encodeEvolve(with: commandEncoder)
        encodeDraw(to: drawable.texture, with: commandEncoder)
        
        commandEncoder.endEncoding()
        commandBuffer.present(drawable)
        commandBuffer.commit()
    }

    public func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {}
}
