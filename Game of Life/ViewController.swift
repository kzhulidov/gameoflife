//
//  ViewController.swift
//  Game of Life
//
//  Created by Konstantin Zhulidov on 15/04/2020.
//  Copyright © 2020 Konstantin Zhulidov. All rights reserved.
//

import Cocoa
import MetalKit

class ViewController: NSViewController {

    @IBOutlet weak var metalView: MTKView!
    @IBOutlet weak var cells: NSTextField!
    var renderer: Renderer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        renderer = Renderer()
        
        metalView.framebufferOnly = false
        //metalView.enableSetNeedsDisplay = true
        metalView.preferredFramesPerSecond = 10
        metalView.device = renderer?.device
        metalView.delegate = renderer
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func onReset(_ sender: Any) {
        let cellCount = Int(cells.intValue)
        renderer?.allocateWorlds(nrInitialCells: cellCount)
    }
}

